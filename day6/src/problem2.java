public class problem2 {
    public static void main(String[] args) {
        System.out.println(pow(2, 3));  // 8
        System.out.println(pow(5, 3));  // 125
        System.out.println(pow(10, 2)); // 100
        System.out.println(pow(2, 5)); // 32
        System.out.println(pow(7, 3));  // 343

    }

    static Integer pow(Integer x, Integer n) {

       int  result = x * x;
        if(n > 2){
            for (int i = 3; i <=n ; i++) {
                result = result * x;
            }
        }
        return result;
    }

}
